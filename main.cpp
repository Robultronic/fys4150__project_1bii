#include <iostream>
#include "lib.h"
#include "time.h"

using namespace std;

int main()
{
    //Initialising, declaring and allocating memory for the matrices:
    int i,j,k;
    double **Arm, **Acm, **B, **C;
    int n = 4500;
    long idum;


    Arm = new double*[n];
    Acm = new double*[n];
    B = new double*[n];
    C = new double*[n];

    for(i=0;i<n;i++) {
        Arm[i] = new double[n];
        Acm[i] = new double[n];
        B[i] = new double[n];
        C[i] = new double[n];
    }

    //Filling matrices B and C with random numbers between 0 and 1:
    for(i=0;i<n;i++) {
        for(j=0;j<n;j++) {
            B[i][j] = ran0(&idum);
            C[i][j] = ran0(&idum);
        }
    }

    //Row-major matrix multiplication and taking the time it takes:
    clock_t start, finish;
    start = clock();
    for(i=0;i<n;i++) {
        for(j=0;j<n;j++) {
            for(k=0;k<n;k++) {
                Arm[i][j] += B[i][k]*C[k][j];
            }
        }
        //cout << i << endl;
    }
    finish = clock();
    cout << "Time (s) taken for row-major:  " << ( (finish - start)/CLOCKS_PER_SEC ) << endl;

    //Write out matrice Arm:
    /*cout << "Row-major A:" << endl;
    for(i=0;i<n;i++) {
        for(j=0;j<n;j++) {
            cout << Arm[i][j] << "     ";
        }
        cout << endl;
    }*/

    /*for(i=0;i<n;i++) {
        delete Arm[i];
    }
    delete Arm;*/

    //Coloumn-major matrix multiplication and taking the time it takes:
    start = clock();
    for(j=0;j<n;j++) {
        for(i=0;i<n;i++) {
            for(k=0;k<n;k++) {
                Acm[i][j] += B[i][k]*C[k][j];
            }
        }
        //cout << j << endl;
    }
    finish = clock();
    cout << "Time (s) taken for coloumn-major:  " << ( (finish - start)/CLOCKS_PER_SEC ) << endl;

    //Write out matrix Acm:
    /*cout << "Coloumn-major A:" << endl;
    for(i=0;i<n;i++) {
        for(j=0;j<n;j++) {
            cout << Acm[i][j] << "     ";
        }
        cout << endl;
    }*/

    //Releasing freeing the memory that had been allocated to the matrices:
    for(i=0;i<n;i++) {
        delete Arm[i];
        delete Acm[i];
        delete B[i];
        delete C[i];
    }
    delete Arm;
    delete Acm;
    delete B;
    delete C;
    return 0;
}
